package ru.tsc.kirillov.tm.api.client;

import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;

public interface IProjectTaskEndpointClient extends IProjectTaskEndpoint, IEndpointClient {

}
