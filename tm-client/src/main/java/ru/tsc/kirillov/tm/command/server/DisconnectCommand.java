package ru.tsc.kirillov.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public final class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            getServiceLocator().getConnectionEndpointClient().disconnect();
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[0];
    }

}
