package ru.tsc.kirillov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.client.IProjectTaskEndpointClient;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;

@NoArgsConstructor
public final class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpointClient {

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull final ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeProjectById(@NotNull final ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull final ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @NotNull
    @Override
    public ProjectBindTaskByIdResponse bindTaskToProjectId(@NotNull final ProjectBindTaskByIdRequest request) {
        return call(request, ProjectBindTaskByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUnbindTaskByIdResponse unbindTaskToProjectId(@NotNull final ProjectUnbindTaskByIdRequest request) {
        return call(request, ProjectUnbindTaskByIdResponse.class);
    }
    
}
