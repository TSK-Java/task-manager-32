package ru.tsc.kirillov.tm.api.client;

import ru.tsc.kirillov.tm.api.endpoint.ISystemEndpoint;

public interface ISystemEndpointClient extends ISystemEndpoint, IEndpointClient {
}
