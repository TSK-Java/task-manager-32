package ru.tsc.kirillov.tm.api.client;

import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;

public interface ITaskEndpointClient extends ITaskEndpoint, IEndpointClient {
}
