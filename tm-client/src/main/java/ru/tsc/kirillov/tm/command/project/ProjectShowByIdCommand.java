package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectShowByIdRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectShowByIdResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по ID]");
        System.out.println("Введите ID проекта:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(id);
        @NotNull final ProjectShowByIdResponse response = getProjectEndpoint().showProjectById(request);
        showProject(response.getProject());
    }

}
