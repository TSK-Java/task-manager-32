package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.TaskShowByIdRequest;
import ru.tsc.kirillov.tm.dto.response.TaskShowByIdResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить задачу по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение задачи по ID]");
        System.out.println("Введите ID задачи:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(id);
        @NotNull final TaskShowByIdResponse response = getTaskEndpoint().showTaskById(request);
        showTask(response.getTask());
    }

}
