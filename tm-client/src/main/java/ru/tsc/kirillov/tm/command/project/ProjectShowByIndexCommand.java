package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ProjectShowByIndexRequest;
import ru.tsc.kirillov.tm.dto.response.ProjectShowByIndexResponse;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectShowCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить проект по его индексу.";
    }

    @Override
    public void execute() {
        System.out.println("[Отображение проекта по индексу]");
        System.out.println("Введите индекс проекта:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(index);
        @NotNull final ProjectShowByIndexResponse response = getProjectEndpoint().showProjectByIndex(request);
        showProject(response.getProject());
    }

}
