package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(@Nullable String login, @Nullable String password);

}
