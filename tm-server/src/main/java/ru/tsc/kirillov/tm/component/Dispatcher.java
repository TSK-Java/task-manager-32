package ru.tsc.kirillov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.endpoint.IOperation;
import ru.tsc.kirillov.tm.dto.request.AbstractRequest;
import ru.tsc.kirillov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, IOperation<?, ?>> map = new LinkedHashMap<>();


    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final IOperation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @Nullable
    public Object call(@NotNull final AbstractRequest request) {
        @NotNull final IOperation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
