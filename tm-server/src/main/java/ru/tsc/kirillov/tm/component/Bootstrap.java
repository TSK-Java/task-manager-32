package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.*;
import ru.tsc.kirillov.tm.api.repository.IDomainRepository;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.endpoint.*;
import ru.tsc.kirillov.tm.repository.DomainRepository;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.util.SystemUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService =  new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonFasterXmlLoadRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonFasterXmlSaveRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonJaxbLoadRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonJaxbSaveRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlJaxbLoadRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlJaxbSaveRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataXmlFasterXmlLoadRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlFasterXmlSaveRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataYamlFasterXmlLoadRequest.class, domainEndpoint::loadDataYamlFasterXml);
        server.registry(DataYamlFasterXmlSaveRequest.class, domainEndpoint::saveDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeTaskStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeTaskStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);

        server.registry(ProjectClearRequest.class, projectTaskEndpoint::clearProject);
        server.registry(ProjectRemoveByIdRequest.class, projectTaskEndpoint::removeProjectById);
        server.registry(ProjectRemoveByIndexRequest.class, projectTaskEndpoint::removeProjectByIndex);
        server.registry(ProjectBindTaskByIdRequest.class, projectTaskEndpoint::bindTaskToProjectId);
        server.registry(ProjectUnbindTaskByIdRequest.class, projectTaskEndpoint::unbindTaskToProjectId);

        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateProfileUser);
        server.registry(UserProfileRequest.class, userEndpoint::viewProfileUser);
    }

    private void prepareShutdown() {
        loggerService.info("** Сервер Task Manager остановлен **");
        backup.stop();
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run() {
        loggerService.info("** Сервер Task Manager запущен **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        backup.start();
        server.start();
    }

}
