package ru.tsc.kirillov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.service.IAuthService;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.api.service.IUserService;
import ru.tsc.kirillov.tm.exception.field.LoginEmptyException;
import ru.tsc.kirillov.tm.exception.field.PasswordEmptyException;
import ru.tsc.kirillov.tm.exception.system.AccessDeniedException;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.util.HashUtil;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null || user.getLocked()) throw new AccessDeniedException();
        @Nullable String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        return user;
    }

}
