package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.UserLoginRequest;
import ru.tsc.kirillov.tm.dto.request.UserLogoutRequest;
import ru.tsc.kirillov.tm.dto.request.UserProfileRequest;
import ru.tsc.kirillov.tm.dto.response.UserLoginResponse;
import ru.tsc.kirillov.tm.dto.response.UserLogoutResponse;
import ru.tsc.kirillov.tm.dto.response.UserProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserProfileResponse profile(@NotNull UserProfileRequest request);

}
