package ru.tsc.kirillov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
