package ru.tsc.kirillov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.User;

public final class UserRegistryResponse extends AbstractUserResponse {

    public UserRegistryResponse(@Nullable final User user) {
        super(user);
    }

}
