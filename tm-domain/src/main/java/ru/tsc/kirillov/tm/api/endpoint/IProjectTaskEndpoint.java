package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.*;
import ru.tsc.kirillov.tm.dto.response.*;

public interface IProjectTaskEndpoint {

    @NotNull
    ProjectClearResponse clearProject(@NotNull ProjectClearRequest request);

    @NotNull
    ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request);

    @NotNull
    ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request);

    @NotNull
    ProjectBindTaskByIdResponse bindTaskToProjectId(@NotNull ProjectBindTaskByIdRequest request);

    @NotNull
    ProjectUnbindTaskByIdResponse unbindTaskToProjectId(@NotNull ProjectUnbindTaskByIdRequest request);

}
