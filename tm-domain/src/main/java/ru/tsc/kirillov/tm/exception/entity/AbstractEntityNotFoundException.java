package ru.tsc.kirillov.tm.exception.entity;

import lombok.NoArgsConstructor;
import ru.tsc.kirillov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            final String message,
            final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
