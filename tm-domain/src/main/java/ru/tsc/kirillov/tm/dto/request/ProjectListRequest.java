package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public ProjectListRequest(@Nullable final Sort sort) {
        this.sort = sort;
    }

}
