package ru.tsc.kirillov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.User;

public final class UserRemoveResponse extends AbstractUserResponse {

    public UserRemoveResponse(@Nullable final User user) {
        super(user);
    }

}
