package ru.tsc.kirillov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectShowByIdRequest extends AbstractIdRequest {

    public ProjectShowByIdRequest(@Nullable final String id) {
        super(id);
    }

}
