package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.AbstractRequest;
import ru.tsc.kirillov.tm.dto.response.AbstractResponse;

@FunctionalInterface
public interface IOperation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    @NotNull
    RS execute(@NotNull RQ request);

}
